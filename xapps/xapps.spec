Name:		xapps
Version:	1.8.1
Release:	1%{?dist}
Summary:	Common components to desktop environments for cross-DE solutions.

License:	GPLv3 and LGPLv3+
URL:		https://github.com/linuxmint/%{name}
Source0:	%{url}/archive/%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	meson
BuildRequires:	python3
BuildRequires:	intltool
BuildRequires:	vala
BuildRequires:	pkgconfig(dbus-1)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gdk-pixbuf-2.0)
BuildRequires:	pkgconfig(cairo)
BuildRequires:	pkgconfig(libgnomekbdui)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:	pkgconfig(dbusmenu-gtk3-0.4)
BuildRequires:	pkgconfig(cairo-gobject)
BuildRequires:	python36-gobject-devel
Requires:		python36-gobject
Requires:		fpaste
Requires:		python3-xapps-overrides%{?_isa} = %{version}-%{release}
Requires:		xdg-utils

Obsoletes:		python2-xapps-overrides < %{version}-%{release}
Provides:		python36-xapps-overrides < %{version}-%{release}

%description
This package includes files that are shared between several XApp
apps (i18n files and configuration schemas).

%package mate
Summary:	Mate status applet with HIDPI support
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description mate
Mate status applet with HIDPI support

%package devel
Summary:	Development libraries and headers for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
Development libraries and headers for developing XApp apps.

%package -n python3-xapps-overrides
Summary:	Python%{python3_version} files for %{name}

BuildRequires:	python3-devel
BuildRequires:	python36-gobject-devel
Requires:		python36-gobject-base%{?_isa}
Requires:		%{name}%{?_isa} = %{version}-%{release}

%description -n python3-xapps-overrides
Python%{python3_version} files for %{name}


%prep
%setup -q

%build
%meson -Ddeprecated_warning=false
%meson_build

%install
%meson_install

%find_lang xapp
%ldconfig_scriptlets

%files -f xapp.lang
%doc README.md AUTHORS
%license COPYING COPYING.LESSER
%{_bindir}/pastebin
%{_bindir}/upload-system-info
%{_bindir}/xfce4-set-wallpaper
%{_datadir}/dbus-1/services/org.x.StatusNotifierWatcher.service
%{_datadir}/glib-2.0/schemas/org.x.apps.gschema.xml
%{_datadir}/icons/hicolor/scalable/*/*.svg
%{_libdir}/girepository-1.0/XApp-1.0.typelib
%{_libdir}/libxapp.so.*
%{_libexecdir}/xapps/sn-watcher/xapp-sn-watcher

%files mate
%{_datadir}/dbus-1/services/org.mate.*
%{_datadir}/mate-panel/applets/*
%{_libexecdir}/xapps/applet*
%{_libexecdir}/xapps/mate*

%files devel
%{_datadir}/gir-1.0/XApp-1.0.gir
%{_datadir}/glade/catalogs/xapp-glade-catalog.xml
%{_datadir}/vala/vapi/xapp.deps
%{_datadir}/vala/vapi/xapp.vapi
%{_includedir}/xapp/*
%{_libdir}/libxapp.so
%{_libdir}/pkgconfig/xapp.pc

%files -n python3-xapps-overrides
%{python3_sitearch}/gi/overrides/XApp.py
%{python3_sitearch}/gi/overrides/__pycache__/XApp.cpython-36.opt-1.pyc
%{python3_sitearch}/gi/overrides/__pycache__/XApp.cpython-36.pyc

%changelog
* Sat Apr 25 2020 Mike Rochefort <mike@michaelrochefort.com> - 1.8.1-1
- Initial Build
